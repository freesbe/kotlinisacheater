package pl.kosobudzki.kotlinisacheater

/**
 * @author krzysztof.kosobudzki
 */
class Preferences {

    private val preferences: Map<String, String> = hashMapOf(
            "one" to "value one",
            "two" to "value two",
            "three" to "value three"
    )

    fun byName(name: String): String = preferences.getOrDefault(name, "")

    fun indexedByName(name: String): String = preferences[name] ?: ""

    fun orElseByName(name: String): String = preferences.getOrElse(name, { "" })
}