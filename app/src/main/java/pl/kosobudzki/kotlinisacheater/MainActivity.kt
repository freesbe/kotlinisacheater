package pl.kosobudzki.kotlinisacheater

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife

/**
 * @author krzysztof.kosobudzki
 */
class MainActivity : AppCompatActivity() {

    @BindView(R.id.preference_one_text_view)
    lateinit var preferenceOneTextView: TextView

    private val preferences: Preferences by lazy { Preferences() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)

        preferenceOneTextView.text = preferences.byName("one")
        preferenceOneTextView.text = preferences.indexedByName("one")
        preferenceOneTextView.text = preferences.orElseByName("one")
    }
}