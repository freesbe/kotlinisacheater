@file:Suppress("IllegalIdentifier")

package pl.kosobudzki.kotlinisacheater

import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * @author krzysztof.kosobudzki
 */
@RunWith(JUnit4::class)
class PreferencesTest {

    private lateinit var systemUnderTests: Preferences

    @Before
    fun setUp() {
        systemUnderTests = Preferences()
    }

    @Test
    fun `Should return empty string when key does not exist`() {
        assertThat(systemUnderTests.byName("not existing key"))
                .isEmpty()
    }

    @Test
    fun `Should return empty string when key does not exist (indexed)`() {
        assertThat(systemUnderTests.indexedByName("not existing key"))
                .isEmpty()
    }

    @Test
    fun `Should return empty string when key does not exist (orElse)`() {
        assertThat(systemUnderTests.orElseByName("not existing key"))
                .isEmpty()
    }
}